# Write an example of simple class
class Dog():
  legs = 4
  def __init__(self, color, size, name, age):
    self.color = color
    self.size = size
    self.name = name
    self.age = age
  
  def say_command (self,command):
    print(' your dog is {}ing'.format(command))
  @staticmethod
  def sleep ():
    print("your dog is sleeping")
  @property
  def say_name_age (self):
    print('your dog name is {} , and it is {} years old'.format (self.name, self.age))



class Dog_girl (Dog):
    def __init__(self,color, size, name, age, gender):
        super(Dog_girl, self).__init__(color, size, name, age)
        self.gender = gender
    
    @property    
    def say_name_age_gender (self):
     print('your dog name is {} , and it is {} years old and it is {} '.format (self.name, self.age, 
    self.gender))
    
    
class Dog_boy (Dog):
    def __init__(self,color, size, name, age, gender):
        super(Dog_boy, self).__init__(color, size, name, age)
        self.gender = gender
    
    def say_name_age_gender (self):
     print('your dog name is {} , and it is {} years old and it is {} '.format (self.name, self.age, 
    self.gender))

__name__ == '__main__'
obj_dict = {'name': 'Chichi', 'age' : 3, 'size' : 4, 'color' : "green" }
print("my dog name is {}, and it is {} , and it is {} cm ".format (obj_dict['name'],
obj_dict['age'], obj_dict['size']))
a = Dog('back', 130, 'Chichi', 1)
a.say_command("sit")
a.sleep()
a.say_name_age
a.say_command("run")

b = Dog_girl('pink', 110, 'Rose',6, 'female')
b.say_name_age_gender

c = Dog_boy ('grey', 130, 'Shurik',9, 'male')
c.say_name_age_gender()
print(c.legs)
print(b.legs)