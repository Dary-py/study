# Task6 write function that takes list as argument find and returns minimum value from this list (with using for loop)
L = [7,9,0,5,4,9,-1,-7,-9]
def min_in_list(x):
  print(min(item for item in x))
  return min(item for item in x)
min_in_list(L)

# Task7 write function that takes list as argument and returns maximum even number (use list comprehension):
def max_even_number(x):
  result= (item for item in x if item % 2 == 0)
  return result
max_even_number(L)

# Task 9 rite function that takes 2 lists as argument and returns   common elements for both lists   - do with using dict or set 
def common_in_lists(list_1,list_2):
  result = (element for element in list_1 if element in list_2)
  return result
common_in_lists([9,8,9,0,6,8,4,3], [1,3,2,2,6])