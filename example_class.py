import random

Horse_name = ["Pockey", "Arrow", "Anna","Blaze", "Chino","Shabash","Midnight", "Bell", "Angy", "Night", "Bolaro", "Acapella", "Pegas", "Fluffy", "Star", "Keya", "Baymont", "Stallion", "Wild", "Maisie"]
coat_color = ["black", "bay", "red", "grey"]
breed = ["Arab", "Frisian", "Welsh", "Hanover", "Icelandic"]
age = [5,8,1,5,6,10,12,20,3]
  
Horses = [{'name' : "Pockey" , 'age' : 3, 'coat_color' : "black", 'breed' : "Arab"}, 
{'name' : "Blaze" , 'age' : 9, 'coat_color' : "black", 'breed' : "Frisian"} , 
{'name' : "Shabash" , 'age' : random.choice(age), 'coat_color' :random.choice(coat_color) , 'breed ': "Icelandic"},
{'name' : "Bell" , 'age' : 2, 'coat_color' :  "red", 'breed' : "Welsh"}, 
{'name ': "Arrow" , 'age' : random.choice(age), 'coat_color' :random.choice(coat_color) , 'breed ': random.choice(breed)},
{'name' :  "Chino", 'age' : random.choice(age), 'coat_color' :random.choice(coat_color) , 'breed ': random.choice(breed)},
{'name' : "Angy" , 'age' : random.choice(age), 'coat_color' :random.choice(coat_color) , 'breed ': random.choice(breed)},
{'name' :  "Anna", 'age' : random.choice(age), 'coat_color' :random.choice(coat_color) , 'breed' : random.choice(breed)},
{'name' : "Bolaro", 'age' : random.choice(age), 'coat_color' :random.choice(coat_color) , 'breed' : random.choice(breed)},
{'name' :  "Midnight", 'age' : random.choice(age), 'coat_color' :random.choice(coat_color) , 'breed' : random.choice(breed)},
  ]
print(Horses)
class Horse:
 def __init__(self, horselist):
  for i in horselist:
   for k, v in i.items():
    setattr(self, k, v)
             
             
  def show_horse (self):
   print("Horse name is '{}' and it is '{}' and it has coat_color - '{}' and breed  - '{}'". format (self.name, self.age, self.coat_color, self.breed))
      
        
  def show_if_color_red(self):
    pass
            
    #        print("'{}' horse has red color".format(self.name))
    #    else:
    #        print("it's not red")
        
        
        
if __name__ == "__main__":
 o = Horse(Horses)
 print(o.name)
 b = Horse(Horses)
 print(b.name)
 print(b.age)
 print(b.coat_color)
 c= Horse ([{'name' : "Pockey" , 'age' : 3, 'coat_color' : "black", 'breed' : "Arab"}, 
 {'name' : "Blaze" , 'age' : 9, 'coat_color' : "black", 'breed' : "Frisian"} ])
 print("**********"*2)
 print(c.name)
 print(c.age)
 print(c.breed)
 c.show_horse()
 b.show_horse()
 o.show_horse()







